Launch-Files und LaunchXML
==========================

Was ist ein Launch-File
--------------------------
Ein Launch-File ist ein Skript, das es ermöglicht
automatisch Nodes zu starten sowie Parameter und
Argumente zu setzen.

Allgemein
--------------------------
Es wird zum Beispiel aus einer Konsole oder aus
einem anderen Launch-File gestartet.
Es folgt einer auf XML basierenden Syntax (LaunchXML).
LaunchXML wird einfach von oben bis unten abgearbeitet
(depth-first). Trotzdem kann man sich nicht auf die
Startreihenfolge verlassen, weil man u.a. nicht weiß,
wann ein Knoten fertig initialisiert ist.

LaunchXML-Tags
--------------------------

	<launch>...</launch>
Rahmentag, der die ganze Datei umspannt.

	<node /> oder <node>...</node>
Startet einen Knoten. Wichtige Attribute dabei:

	name="BELIEBIGER EINDEUTIGER NAME"
	type="NAME DER NODE"
	pkg="NAME DES PAKETS"
	args="ARGUMENT1 ARGUMENT2"
	output="log/screen"
*screen* bedeutet, dass Ausgaben in die Konsole geschrieben werden,
anstatt, wie sonst, nur in ein log-File.

	<include file="PFAD ZU ANDEREM LAUNCH-FILE">...</include>
Bindet ein anderes Launch-File ein (=> startet es).
Argumente, die innerhalb definiert werden, werden übergeben.

	<arg name="NAME" value="WERT" />
Definiert ein Argument. Wird der Wert anstatt mit *value*,
mit *default* gesetzt, kann er überschrieben werden.
Eingebundene Skripte können um (an sie übergebene) Argumente
aufzufangen, das Argument ohne Wertzuweisung deklarieren.

	<param name="NAME" type="int/double/string/bool" value="WERT" />
Setzt einen Parameter in den Parameterserver. Je nachdem,
wo das steht, wird der Parameter zum Beispiel im privaten
Namespace eines Knoten gesetzt.

	if="BOOLEANWERT" unless="BOOLEANWERT"
Zusätzliche Attribute, die das Aufrufen eines Tags abhängig
von einem Booleanwert machen.

	$(arg NAME)
Liest ein Argument aus.

	$(find PAKETNAME)
Liest den Basispfad eines Pakets aus.

Beispiele
------------------------
Beispiele aus dem Vortrag im Ordner ./launch
